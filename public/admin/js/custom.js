$(document).ready( function () {
    $('#current_password').keyup( function () {
        var current_password = $('#current_password').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '/admin/check-current-password',
            data: {current_password:current_password},
            success: function (response){
                if (response == 'false') {
                    $('#verify_current_password').html('Current Password is Incorrect!')
                } else if (response == 'true') {
                    $('#verify_current_password').html('Current Password is Correct')
                }
            }, error: function () {
                alert('Error')
            }
        })
    })

})
