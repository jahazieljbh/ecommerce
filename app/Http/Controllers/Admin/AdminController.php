<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function dashboard() {
        return view('admin.dashboard');
    }

    public function login(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'email' => 'required|email|max:255',
                'password' => 'required|max:30'
            ];

            $messages = [
                'email.required' => 'Email is required',
                'email.email' => 'Valid Email is required',
                'password.required' => 'Password is required',
            ];

            $this->validate($request, $rules, $messages);

            if (Auth::guard('admin')
                ->attempt(['email'=>$data['email'], 'password'=>$data['password']])) {
                return redirect('admin/dashboard');
            } else {
                return redirect()->back()->with('error_message', 'Invalid Email or Password');
            }
        }
        return view('admin.login');
    }

    public function updatePassword(Request $request) {
        if ($request->isMethod('post')) {
            $data = $request->all();
            // Check if the current password is correct
            $currentPasswordIsCorrect = Hash::check($data['current_password'], Auth::guard('admin')->user()->password);

            if ($currentPasswordIsCorrect) {
                // Check if the new password and confirmation match
                if ($data['new_password'] == $data['confirm_password']) {
                    // Update the password if they match
                    Admin::where('id', Auth::guard('admin')->user()->id)
                        ->update(['password' => bcrypt($data['new_password'])]);
                    return redirect()->back()->with('success_message', 'Password updated successfully!');
                } else {
                    // Redirect with an error message if the new password and confirmation do not match
                    return redirect()->back()->with('error_message', 'Your New password and Retype password do not match.');
                }
            } else {
                // Redirect with an error message if the current password is incorrect
                return redirect()->back()->with('error_message', 'Your current password is incorrect!');
            }

        }
        return view('admin.update_password');
    }

    public function checkCurrentPassword(Request $request) {
        $data =$request->all();
        return Hash::check($data['current_password'], Auth::guard('admin')->user()->password) ? 'true' : 'false';
    }

    public function updateAdminDetails(Request $request) {
        if ($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'admin_name' => 'required|max:255',
                'admin_email' => 'required|max:255',
                'admin_mobile' => 'required|numeric'
            ];

            $messages = [
                'admin_name.required' => 'Name is required',
                'admin_mobile.required' => 'Mobile number is required',
                'admin_mobile.numeric' => 'Valid Mobile is required',
            ];

            $this->validate($request, $rules, $messages);

            Admin::where('email', Auth::guard('admin')->user()->email)->update(['name'=>$data['admin_name'], 'mobile'=>$data['admin_mobile']]);

            return redirect()->back()->with('success_message', 'Admin details has been updated successfully!');
        }
        return view('admin.update_details');
    }

    public function logout() {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
}
